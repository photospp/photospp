cmake_minimum_required(VERSION 3.6.0 FATAL_ERROR)
#----------------------------------------------------------------------------
project(Photospp LANGUAGES CXX C Fortran)
set(PROJECT_VERSION 3.65)
set(Photospp_VERSION_MAJOR 3)
set(Photospp_VERSION_MINOR 65)
set(PACKAGE_VERSION ${PROJECT_VERSION})
set(PHOTOS_VERSION_CODE 365)
set(CMAKE_VERBOSE_MAKEFILE OFF)
#This module respects HFS, e.g. defines lib or lib64 when it is needed.
include("GNUInstallDirs")
include("CMakePrintHelpers")
set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake/Modules ${CMAKE_MODULE_PATH})

option(Photospp_ENABLE_HEPMC2      "Enables building of with HepMC2 library" ON)
option(Photospp_ENABLE_HEPMC3      "Enables building of with HepMC3 library" ON)
option(Photospp_ENABLE_EXAMPLES    "Enables building of examples and testing them" ON)
option(Photospp_ENABLE_MC-TESTER   "Enables building of examples with MC-TESTER and testing them" OFF)

message(STATUS "${PROJECT_NAME}: CMAKE_CXX_COMPILER_ID ${CMAKE_CXX_COMPILER_ID}" )
message(STATUS "${PROJECT_NAME}: CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} ${CMAKE_CXX_FLAGS_${CMAKE_BUILD_TYPE}}  " )
message(STATUS "${PROJECT_NAME}: Photospp_ENABLE_HEPMC2=${Photospp_ENABLE_HEPMC2}" )
message(STATUS "${PROJECT_NAME}: Photospp_ENABLE_HEPMC3=${Photospp_ENABLE_HEPMC3}" )
message(STATUS "${PROJECT_NAME}: Photospp_ENABLE_EXAMPLES=${Photospp_ENABLE_EXAMPLES}" )
message(STATUS "${PROJECT_NAME}: Photospp_ENABLE_MC-TESTER=${Photospp_ENABLE_MC-TESTER}" )

set(components  X HEPEVT)
if(Photospp_ENABLE_HEPMC3 )
  if (HepMC3_DIR  OR HEPMC3_DIR)
    find_package(HepMC3 REQUIRED PATHS ${HepMC3_DIR} ${HEPMC3_DIR} PATH_SUFFIXES . share/HepMC3/cmake share cmake NO_DEFAULT_PATH)
  else()
    find_package(HepMC3 REQUIRED) 
  endif()
  message(STATUS "${PROJECT_NAME}: HepMC3_FOUND=${HepMC3_FOUND} HepMC3_VERSION=${HepMC3_VERSION}")
  cmake_print_properties(TARGETS HepMC3::HepMC3 PROPERTIES LOCATION INTERFACE_INCLUDE_DIRECTORIES)
  list(APPEND components HepMC3)
endif()
if(Photospp_ENABLE_HEPMC2)
  find_package(HepMC REQUIRED)
  message(STATUS "${PROJECT_NAME}: HepMC_FOUND=${HepMC_FOUND} HEPMC_VERSION=${HEPMC_VERSION}")
  cmake_print_properties(TARGETS HepMC::HepMC PROPERTIES LOCATION INTERFACE_INCLUDE_DIRECTORIES)
  list(APPEND components HepMC)
endif()

set(Photospp_src 
src/photos-C/photosC.cxx src/photos-C/forZ-MEc.cxx src/photos-C/forW-MEc.cxx src/photos-C/pairs.cxx src/photos-C/HEPEVT_struct.cxx
src/utilities/Log.cxx  src/utilities/PhotosDebugRandom.cxx  src/utilities/PhotosRandom.cxx  src/utilities/PhotosUtilities.cxx
src/photosCInterfaces/PhotosBranch.cxx  src/photosCInterfaces/Photos.cxx  src/photosCInterfaces/PhotosEvent.cxx  src/photosCInterfaces/PhotosParticle.cxx
)
set(Photospp_includes
src/photos-C/forW-MEc.h  src/photos-C/forZ-MEc.h  src/photos-C/HEPEVT_struct.h  src/photos-C/pairs.h  src/photos-C/photosC.h
src/utilities/Log.h  src/utilities/PhotosDebugRandom.h  src/utilities/PhotosRandom.h  src/utilities/PhotosUtilities.h
src/photosCInterfaces/PhotosBranch.h  src/photosCInterfaces/PhotosEvent.h  src/photosCInterfaces/Photos.h  src/photosCInterfaces/PhotosParticle.h
)
set(PhotosppHepMC3_src 
src/eventRecordInterfaces/PhotosHepMC3Particle.cxx src/eventRecordInterfaces/PhotosHepMC3Event.cxx
)
set(PhotosppHepMC3_includes 
src/eventRecordInterfaces/PhotosHepMC3Particle.h src/eventRecordInterfaces/PhotosHepMC3Event.h
)

set(PhotosppHepMC_src 
src/eventRecordInterfaces/PhotosHepMCParticle.cxx src/eventRecordInterfaces/PhotosHepMCEvent.cxx
)
set(PhotosppHepMC_includes
src/eventRecordInterfaces/PhotosHepMCParticle.h src/eventRecordInterfaces/PhotosHepMCEvent.h
)

set(PhotosppHEPEVT_src 
src/eventRecordInterfaces/PhotosHEPEVTParticle.cxx src/eventRecordInterfaces/PhotosHEPEVTEvent.cxx
)
set(PhotosppHEPEVT_includes 
src/eventRecordInterfaces/PhotosHEPEVTEvent.h
src/eventRecordInterfaces/PhotosHEPEVTParticle.h
)

configure_file(${PROJECT_SOURCE_DIR}/src/photosCInterfaces/Version.h.in  ${PROJECT_BINARY_DIR}/include/Photos/Version.h)
install(FILES ${PROJECT_BINARY_DIR}/include/Photos/Version.h DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/Photos COMPONENT devel)
foreach (c ${components})
  if (${c} STREQUAL "X")
   set(c "")
  endif()
  foreach(myinclude IN  LISTS Photospp${c}_includes)
    get_filename_component(x ${myinclude} NAME)
    configure_file(${myinclude}  ${PROJECT_BINARY_DIR}/include/Photos/${x} COPYONLY)
  endforeach()
  list(TRANSFORM Photospp${c}_src PREPEND ${PROJECT_SOURCE_DIR}/)
  list(TRANSFORM Photospp${c}_includes PREPEND ${PROJECT_SOURCE_DIR}/)
  add_library(Photospp${c} SHARED  ${Photospp${c}_src})
  SET_TARGET_PROPERTIES (Photospp${c}  PROPERTIES VERSION ${PROJECT_VERSION} SOVERSION ${PROJECT_VERSION} EXPORT_NAME Photospp::Photospp${c})
  target_include_directories(Photospp${c} PUBLIC  
                                         $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/include>
                                         $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/include/Photos>
                                         $<INSTALL_INTERFACE:include/Photos>)
  INSTALL (TARGETS  Photospp${c}  EXPORT PhotosppTargets DESTINATION ${CMAKE_INSTALL_LIBDIR})
  add_library(Photospp::Photospp${c} ALIAS Photospp${c})

  add_library(Photospp${c}_static STATIC  ${Photospp${c}_src})
  SET_TARGET_PROPERTIES (Photospp${c}_static  PROPERTIES OUTPUT_NAME Photospp${c} EXPORT_NAME Photospp::Photospp${c}_static)
  target_include_directories(Photospp${c}_static PUBLIC  
                                         $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/include>
                                         $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/include/Photos>
                                        $<INSTALL_INTERFACE:include/Photos>)
  INSTALL (TARGETS  Photospp${c}_static EXPORT PhotosppTargets DESTINATION ${CMAKE_INSTALL_LIBDIR})
  add_library(Photospp::Photospp${c}_static ALIAS Photospp${c}_static)
  install(FILES ${Photospp${c}_includes} DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/Photos COMPONENT devel)
endforeach()

if (Photospp_ENABLE_EXAMPLES)
  ENABLE_TESTING()
  add_subdirectory(examples)
endif()
include(CMakePackageConfigHelpers)
set(CONFIG_INSTALL_DIR ${CMAKE_INSTALL_DATADIR}/Photospp/cmake)
configure_package_config_file(cmake/Templates/PhotosppConfig.cmake.in
  ${CMAKE_CURRENT_BINARY_DIR}/outputs/share/Photospp/cmake/PhotosppConfig.cmake
  INSTALL_DESTINATION ${CONFIG_INSTALL_DIR}
  PATH_VARS CMAKE_INSTALL_INCLUDEDIR
            CMAKE_INSTALL_LIBDIR
            )

write_basic_package_version_file(${CMAKE_CURRENT_BINARY_DIR}/outputs/share/Photospp/cmake/PhotosppConfig-version.cmake
  COMPATIBILITY SameMajorVersion VERSION ${PROJECT_VERSION})

install(FILES ${CMAKE_CURRENT_BINARY_DIR}/outputs/share/Photospp/cmake/PhotosppConfig.cmake
              ${CMAKE_CURRENT_BINARY_DIR}/outputs/share/Photospp/cmake/PhotosppConfig-version.cmake
        DESTINATION ${CONFIG_INSTALL_DIR} COMPONENT devel)


install(EXPORT PhotosppTargets DESTINATION ${CONFIG_INSTALL_DIR} COMPONENT devel)
