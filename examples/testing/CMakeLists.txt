find_package(LATEX COMPONENTS PDFLATEX REQUIRED)
message(STATUS "${PROJECT_NAME}: LATEX_FOUND=${LATEX_FOUND} PDFLATEX_COMPILER=${PDFLATEX_COMPILER}")

set(EVENTS  1000)
set(ALL_MODE 0)
set(PI_MODE 3)
set(RHO_MODE 4)


add_executable(tauola_test photos_tauola_test.cxx)
set_source_files_properties(tauola_test photos_tauola_test.cxx PROPERTIES LANGUAGE CXX )
target_link_libraries(tauola_test PRIVATE 
Tauolapp::TauolaCxxInterface 
                                          Tauolapp::TauolaHepMC
                                          Tauolapp::TauolaFortran 
                                          Tauolapp::TauolaHEPEVT
                                          Pythia8::Pythia8 
                                          HepMC::HepMC
                                          ROOT::Core
                                          MC-TESTER::MCTester
                                          MC-TESTER::HepMCEvent
                                          Photospp::Photospp
                                          Photospp::PhotosppHepMC
                                          )
                                          
add_executable(photos_test photos_test.cxx)
set_source_files_properties(photos_test photos_test.cxx PROPERTIES LANGUAGE CXX )
target_link_libraries(photos_test PRIVATE 
                                          Pythia8::Pythia8 
                                          HepMC::HepMC
                                          ROOT::Core
                                          MC-TESTER::MCTester
                                          MC-TESTER::HepMCEvent
                                          Photospp::Photospp
                                          Photospp::PhotosppHepMC
                                          )

macro(mc_tester_compare X PROG FILE1  SETUP UA CONF MODE FLOATVAL)
  set(Y ${CMAKE_CURRENT_BINARY_DIR}/${X})
  file(MAKE_DIRECTORY  ${PROJECT_BINARY_DIR}/MC-TESTER)
  set(FILE2 ${CMAKE_CURRENT_BINARY_DIR}/${X}/mc-tester.root)
  file(MAKE_DIRECTORY ${Y})
  file(WRITE ${Y}/rootlogon.C  "\
{\n\
gSystem->Load(\"libHist.so\");\n\
gSystem->Load(\"libGpad.so\");\n\
gSystem->Load(\"${MC-TESTER_LIBRARIES_DIR}/libHEPEvent.so\");\n\
gSystem->Load(\"${MC-TESTER_LIBRARIES_DIR}/libMCTester.so\");\n\
gROOT->SetStyle(\"Plain\");\n\
}\n")

  configure_file(${SETUP} ${Y}/SETUP.C )
  file(COPY ${UA} DESTINATION ${Y} )
  configure_file(${MC-TESTER_ANALYZE_DIR}/BOOKLET.C ${Y}/BOOKLET.C )
  configure_file(${MC-TESTER_ANALYZE_DIR}/ANALYZE.C ${Y}/ANALYZE.C )
  configure_file(${MC-TESTER_ANALYZE_DIR}/tester.tex ${Y}/tester.tex )

  add_test(NAME ${X}_GENERATION COMMAND  ${PROG} ${CONF} 0 ${EVENTS} ${MODE} ${FLOATVAL} WORKING_DIRECTORY ${Y})  
  add_test(NAME ${X}_ANALYZE COMMAND ${ROOT_root_CMD} -b -q "ANALYZE.C++(\"${Y}\",\"${FILE1}\",\"${FILE2}\")"  WORKING_DIRECTORY ${Y})  
  add_test(NAME ${X}_BOOKLET COMMAND ${ROOT_root_CMD} -b -q "BOOKLET.C(\"${Y}\")"   WORKING_DIRECTORY ${Y})  

  add_test(NAME ${X}_PDFLATEX_1 COMMAND ${PDFLATEX_COMPILER} tester.tex WORKING_DIRECTORY ${Y})  
  add_test(NAME ${X}_PDFLATEX_2 COMMAND ${PDFLATEX_COMPILER} tester.tex WORKING_DIRECTORY ${Y})  
  add_test(NAME ${X}_PDFLATEX_3 COMMAND ${PDFLATEX_COMPILER} tester.tex WORKING_DIRECTORY ${Y})  
  add_test(NAME ${X}_COPY_PDF COMMAND ${CMAKE_COMMAND} -E copy tester.pdf  ${PROJECT_BINARY_DIR}/MC-TESTER/${X}.pdf  WORKING_DIRECTORY ${Y})  
      
  set_tests_properties(${X}_ANALYZE PROPERTIES DEPENDS ${X}_GENERATION)
  set_tests_properties(${X}_BOOKLET PROPERTIES DEPENDS ${X}_ANALYZE)
  set_tests_properties(${X}_PDFLATEX_1 PROPERTIES DEPENDS ${X}_BOOKLET)
  set_tests_properties(${X}_PDFLATEX_2 PROPERTIES DEPENDS ${X}_PDFLATEX_1)
  set_tests_properties(${X}_PDFLATEX_3 PROPERTIES DEPENDS ${X}_PDFLATEX_2)
  set_tests_properties(${X}_COPY_PDF PROPERTIES DEPENDS ${X}_PDFLATEX_3)
  set_tests_properties(${X}_GENERATION PROPERTIES ENVIRONMENT "ROOT_INCLUDE_PATH=${MC-TESTER_INCLUDE_DIR};MCTESTERLOCATIONLIB=${MC-TESTER_LIBRARIES_DIR}")
  set_tests_properties(${X}_ANALYZE PROPERTIES ENVIRONMENT "ROOT_INCLUDE_PATH=${MC-TESTER_INCLUDE_DIR};MCTESTERLOCATIONLIB=${MC-TESTER_LIBRARIES_DIR}")
  set_tests_properties(${X}_BOOKLET PROPERTIES ENVIRONMENT "ROOT_INCLUDE_PATH=${MC-TESTER_INCLUDE_DIR};MCTESTERLOCATIONLIB=${MC-TESTER_LIBRARIES_DIR}")
endmacro()

add_subdirectory(Htautau)
add_subdirectory(ScalNLO)
add_subdirectory(Wenu)
add_subdirectory(WmunuNLO)
add_subdirectory(Zmumu)
add_subdirectory(Ztautau)
add_subdirectory(pairs)
add_subdirectory(ttbar)
add_subdirectory(Wmunu)
add_subdirectory(Zee)
add_subdirectory(ZmumuNLO)



