# - Locate Tauola
# Defines:
#
#  Tauolapp_FOUND
#  Tauolapp_VERSION
#  Tauolapp_INCLUDE_DIR
#  Tauolapp_INCLUDE_DIRS (not cached)
#  Tauolapp_LIBRARY
#  Tauolapp_LIBRARIES (not cached) 
if (TAUOLAPP_ROOT_DIR OR TAUOLAPP_DIR OR Tauolapp_DIR  OR (DEFINED ENV{TAUOLAPP_ROOT_DIR}) OR (DEFINED ENV{TAUOLAPP_DIR}) )
  set(TAUOLAPP_SEARCH_DIRS "" CACHE STRING "" FORCE)
  if (TAUOLAPP_ROOT_DIR)
    list (APPEND TAUOLAPP_SEARCH_DIRS "${TAUOLAPP_ROOT_DIR}" )
  endif()
  if (TAUOLAPP_DIR)
    list (APPEND TAUOLAPP_SEARCH_DIRS "${TAUOLAPP_DIR}" )
  endif()
  if (Tauolapp_DIR)
    list (APPEND TAUOLAPP_SEARCH_DIRS "${Tauolapp_DIR}" )
  endif()
  if (DEFINED ENV{TAUOLAPP_ROOT_DIR})
    list (APPEND TAUOLAPP_SEARCH_DIRS "$ENV{TAUOLAPP_ROOT_DIR}" )
  endif()
  if (DEFINED ENV{TAUOLAPP_DIR})
    list (APPEND TAUOLAPP_SEARCH_DIRS "$ENV{TAUOLAPP_DIR}" )
  endif()
endif()

message(STATUS "${PROJECT_NAME}:  TAUOLAPP_SEARCH_DIRS=${TAUOLAPP_SEARCH_DIRS}")

if (TAUOLAPP_SEARCH_DIRS)
  find_path(Tauolapp_INCLUDE_DIR NAMES  Tauola/Tauola.h PATHS ${TAUOLAPP_SEARCH_DIRS}  PATH_SUFFIXES . include NO_DEFAULT_PATH)
else()
  find_path(Tauolapp_INCLUDE_DIR NAMES  Tauola/Tauola.h PATH_SUFFIXES . include)
endif()

if(NOT Tauolapp_FIND_COMPONENTS)
  set(Tauolapp_FIND_COMPONENTS Fortran CxxInterface HepMC HepMC3)
endif()
if(Tauolapp_INCLUDE_DIR)
  set(Tauolapp_FOUND TRUE)
  foreach(component ${Tauolapp_FIND_COMPONENTS})
    if (TAUOLAPP_SEARCH_DIRS)
      find_library(Tauolapp_${component}_LIBRARY NAMES Tauola${component} PATHS ${TAUOLAPP_SEARCH_DIRS}  PATH_SUFFIXES lib64 lib NO_DEFAULT_PATH)
    else()
      find_library(Tauolapp_${component}_LIBRARY NAMES Tauola${component} PATH_SUFFIXES lib64 lib)    
    endif()
    if (Tauolapp_${component}_LIBRARY)
      set(Tauolapp_${component}_FOUND TRUE)
      list(APPEND Tauolapp_LIBRARIES ${Tauolapp_${component}_LIBRARY})
      get_filename_component(libdir ${Tauolapp_${component}_LIBRARY} PATH)
    else()
    set(Tauolapp_${component}_FOUND FALSE)
    if (Tauolapp_FIND_REQUIRED_${component})
      set(Tauolapp_FOUND FALSE)
    endif()
  endif()
  message(STATUS "Tauolapp_${component}_FOUND=${Tauolapp_${component}_FOUND}")
  mark_as_advanced(Tauolapp_${component}_LIBRARY)
  endforeach()
endif()
set(Tauolapp_VERSION 1.1.8)#FIXME

# handle the QUIETLY and REQUIRED arguments and set Tauolapp_FOUND to TRUE if
# all listed variables are TRUE

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Tauolapp REQUIRED_VARS Tauolapp_INCLUDE_DIR  Tauolapp_LIBRARIES VERSION_VAR Tauolapp_VERSION HANDLE_COMPONENTS)


if(Tauolapp_FOUND)
  foreach(component ${Tauolapp_FIND_COMPONENTS})
    if (NOT TARGET Tauolapp::Tauola${component})
      add_library(Tauolapp::Tauola${component} UNKNOWN IMPORTED)
      set_target_properties(Tauolapp::Tauola${component} PROPERTIES
        IMPORTED_LOCATION "${Tauolapp_${component}_LIBRARY}"
        INTERFACE_INCLUDE_DIRECTORIES "${Tauolapp_INCLUDE_DIR}"
     )
    endif()
  endforeach()
endif()


mark_as_advanced(Tauolapp_FOUND Tauolapp_INCLUDE_DIR )

